//
//  Extensions.swift
//  MyTestApp
//
//  Created by Камила Артыкова on 03.08.2023.
//

import Foundation
import SwiftUI

extension Text {
    func lilacTitle() -> some View {
        self.font(.title)
            .fontWeight(.heavy)
            .foregroundColor(Color("AccentColor"))

    }
}
