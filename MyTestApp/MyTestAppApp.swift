//
//  MyTestAppApp.swift
//  MyTestApp
//
//  Created by Камила Артыкова on 03.08.2023.
//

import SwiftUI

@main
struct MyTestAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
