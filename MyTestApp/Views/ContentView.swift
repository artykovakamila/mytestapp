//
//  ContentView.swift
//  MyTestApp
//
//  Created by Камила Артыкова on 03.08.2023.
//

import SwiftUI
import Combine

struct ContentView: View {
    @StateObject var triviaManager = TriviaManager()

    var body: some View {
        NavigationView {
            VStack(spacing: 40) {
                VStack(spacing: 20) {
                    Text("Trivia Game")
                        .lilacTitle()

                    Text("Are you ready to test out your trivia skills?")
                        .foregroundColor(Color("AccentColor"))
                }
                NavigationLink {
                    TriviaView()
                        .environmentObject(triviaManager)
                } label: {
                    PrimaryButton(text: "Let's go!")
                }
            }

            .frame(maxWidth: .infinity, maxHeight: .infinity)
            .edgesIgnoringSafeArea(.all)
            .background(Color("BackgroundColor"))
        }
    }
}

        struct ContentView_Previews: PreviewProvider {
            static var previews: some View {
                ContentView()
    }
}
