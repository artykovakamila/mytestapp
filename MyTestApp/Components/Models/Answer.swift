//
//  Answer.swift
//  MyTestApp
//
//  Created by Камила Артыкова on 03.08.2023.
//

import Foundation

struct Answer: Identifiable {
    var id = UUID()
    var text: AttributedString
    var isCorrect: Bool
}
